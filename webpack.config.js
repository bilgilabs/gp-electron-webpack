const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.config');

module.exports = (env, options) => {
    const webpackConfig = baseConfig(env, options);

    return merge.smart(webpackConfig, {
        optimization: {
            nodeEnv: 'web'
        },
        target: 'web'
    });
};