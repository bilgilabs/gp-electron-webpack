Wenn ihr die nächste Zeile 
```
export PATH=$PATH:./node_modules/.bin/
```

permanent in eure
```
$HOME/.bashrc
```   
reinpackt, dann könnt ihr direkt aus der Kommandozeile automatisch auf die Programme/Skripte, die in ***./node_modules/.bin*** liegen, zugreifen.

```
npm install         
```

Um eure App im Browser zu starten (mit Hot-Reloading, d.h. immer, wenn ihr eine Änderung an einer eurer Dateien durchführt, lädt sich die App im Browser neu), führt ihr

```
npm run browser
```

aus.

Um eure App in Electron zu starten (mit Hot-Reloading), führt ihr

```
npm run electron
```

durch. 

Wenn ihr kein Hot-Reloading haben und nach Änderungen selbst die Seite neuladen wollt, dann ruft ihr ***npm run browser-cold*** respektive ***npm run electron-cold*** auf.
