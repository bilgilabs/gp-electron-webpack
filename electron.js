const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require("path");
const isDevelopment = require("electron-is-dev");
let mainWindow;
function createWindow() {
    mainWindow = new BrowserWindow({ width: 1140, height: 800 });
    if( !isDevelopment ) {
        mainWindow.loadURL( `http://${ process.env.ELECTRON_WEBPACK_WDS_HOST }:${ process.env.ELECTRON_WEBPACK_WDS_PORT }` );
    } else {
        mainWindow.loadFile( path.resolve( __dirname, './dist/index.html' ) );
    }

    // mainWindow.loadURL(
    //     isDev
    //         ? "http://localhost:8080"
    //         : `file://${path.join(__dirname, "./dist/index.html")}`
    // );
    mainWindow.on("closed", () => (mainWindow = null));
}
app.on("ready", createWindow);
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});
app.on("activate", () => {
    if (mainWindow === null) {
        createWindow();
    }
});
