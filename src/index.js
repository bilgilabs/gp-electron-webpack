
// die nächste anweisung importiert nur die bootstrap javascript dateien
import 'bootstrap';

// eine einfache .css datei
import './style.css';

// entweder laden wir konfigurierbares bootstrap-stylesheets rein
import './bootstrap-custom.scss'

// oder wir könnten einfach die vorkonfigurierten bootstrap-stylesheets reinladen
// import 'bootstrap/dist/css/bootstrap.min.css';

import './style-sass.scss';

import $ from 'jquery'

import navLinks from './navLinks.html';
import contentRechts from './contentRechts.html';

// hier fügen wir das html aus main.html ins body der seite ein

$(function() {
    alert("Jetzt ist die DOM Tree auch initialisiert und die linke und rechte Spalte werden nachgeladen und den DOM Tree injiziert.")
    $('#navLinks').html(navLinks)
    $('#contentRechts').html(contentRechts)
})


