const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
// var InjectHtmlPlugin = require('inject-html-webpack-plugin')

module.exports = (env,options) => {
    return {
        entry: './src/index.js',
        mode: 'development',
        devtool: "inline-source-map",
        devServer: {
            contentBase: './dist',
            inline: true,
            port: 4000
        },
        module: {
            rules: [
                {
                    test: /\.s[ac]ss$/i,
                    use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
                },
                {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.html$/i,
                    loader: 'html-loader',
                }
            ]
        },
        output: {
            path: path.resolve(__dirname, './dist'),
            filename: 'index_bundle.min.js',
        },
        plugins: [
            new HtmlWebpackPlugin({
                title: 'My App',
                template: 'src/app.html'
            }),
            new MiniCssExtractPlugin()
            // new InjectHtmlPlugin({
            //     filename: './index.html',
            //     chunks: ['index'],
            //     transducer:"http://cdn.example.com",
            //     custom: [{
            //         start: '<!-- start:bundle-time -->',
            //         end: '<!-- end:bundle-time -->',
            //         content: Date.now()
            //     }]
            // })
        ]
    }
};
